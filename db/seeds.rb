# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Feedback.destroy_all
Pitch.destroy_all
User.destroy_all

maxime = User.create!( email:"maxime@e2.is",  password: "password", password_confirmation: "password", new:"", fbid:2147483647, funame:"Maxime", fname:"Maxime Paul", username:"maxap", registered: true, created_at: Time.zone.now)
stephanie = User.create!( email:"slghosto@gmail.com", password: "password", password_confirmation: "password", new:"", fbid:2147483647, funame:"Stephanie", fname:"Stephanie L. Ghoston", username:"slghosto", registered: true, created_at: Time.zone.now)
kanwal = User.create!( email:"shumailmb@yahoo.com", password: "password", password_confirmation: "password", new:"", fbid:2147483647, funame:"Kanwal", fname:"Kanwal Fatima", username:"tester", registered: true, created_at: Time.zone.now)
julius = User.create!( email:"juliusrainey@gmail.com", password: "password", password_confirmation: "password", new:"", fbid:2147483647, funame:"Julius", fname:"Julius Rainey", username:"one9ooh6", registered: true, created_at: Time.zone.now)
tobias = User.create!( email:"tobias@komplekscreative.com", password: "password", password_confirmation: "password", new:"", fbid:2147483647, funame:"Tobias", fname:"Tobias Rose", username:"", registered: true, created_at: Time.zone.now)
brandon = User.create!( email:"brandontluong@gmail.com", password: "password", password_confirmation: "password", new:"", fbid:2147483647, funame:"Brandon", fname:"Brandon T. Luong", username:"master", registered: true, created_at: Time.zone.now)
ace = User.create!( email:"alston.clark17@gmail.com", password: "password", password_confirmation: "password", new:"", fbid:2147483647, funame:"Ace", fname:"Ace Clark", username:"Alston", registered: true, created_at: Time.zone.now)
liz = User.create!( email:"Lizakins12@gmail.com", password: "password", password_confirmation: "password", new:"", fbid:2147483647, funame:"Liz", fname:"Liz Akins", username:"dark_blaze", registered: true, created_at: Time.zone.now)
larry = User.create!( email:"larrywilliamsjr87@gmail.com", password: "password", password_confirmation: "password", new:"", fbid:2147483647, funame:"Larry", fname:"Larry Jr.", username:"poweruser", registered: true, created_at: Time.zone.now)
brian = User.create!( email:"bkl82@me.com", password: "password", password_confirmation: "password", new:"", fbid:2147483647, funame:"Brian", fname:"Brian Keith Lee", username:"briguy82", registered: true, created_at: Time.zone.now)
nic = User.create!( email:"nic@smallnic.com", password: "password", password_confirmation: "password", new:"", fbid:2147483647, funame:"Nic", fname:"Nic Small", username:"Nic", registered: true, created_at: Time.zone.now)

fndrs = Pitch.create!( innovation_name:"#000000 fndrs", problem:"How to connect minority startup founders together for more success.", pitch:"hOV4Gsumb8o", stage:"Product/Market Fit", needs:"Funding", love:3, feedback_total:5, stars_total:5, stars_avg:0.700002, views:50, user_id:maxime.id, created_at:Time.zone.now )
testing3 = Pitch.create!( innovation_name:"Testing3", problem:"How to find your favorite show when you don''t have cable.", pitch:"HEi0U8ZAtNA", stage:"Concept", needs:"Engineers to build prototype.", love:-2, feedback_total:27, stars_total:64, stars_avg:4.33125, views:34, user_id:maxime.id, created_at:Time.zone.now )
testing4 = Pitch.create!( innovation_name:"Testing4", problem:"How to still support your favorite musicians, but want to know if the songs are good on the CD.", pitch:"dlezSiH_1KY", stage:"Prototype", needs:"A strong testing group.", love:3, feedback_total:12, stars_total:43, stars_avg:3.46976, views:14, user_id:maxime.id, created_at:Time.zone.now )
testing5 = Pitch.create!( innovation_name:"Testing5", problem:"How to get medicine when you''re sick and can''t get up.", pitch:"djakFYYoOck", stage:"Idea", needs:"Need to set up the business", love:66, feedback_total:86, stars_total:240, stars_avg:2.2925, views:120, user_id:maxime.id, created_at:Time.zone.now )
wildness = Pitch.create!( innovation_name:"Wildness", problem:"When you feel bored and need to mix it up.", pitch:"pdFCUx5qXk0", stage:"Concept", needs:"More research", love:-3, feedback_total:3, stars_total:3, stars_avg:1.5, views:5, user_id:maxime.id, created_at:Time.zone.now )
crunchii = Pitch.create!( innovation_name:"Crunchii - Registration", problem:"Financial Data prediction", pitch:"https://pitchlove-me", stage:"Idea", needs:"Majestic unicorns", love:0, feedback_total:0, stars_total:0, stars_avg:0, views:0, user_id:maxime.id, created_at:Time.zone.now )
tester = Pitch.create!( innovation_name:"tester", problem:"Water Purification", pitch:"0", stage:"Idea", needs:"We will purify water using organic material", love:0, feedback_total:0, stars_total:0, stars_avg:0, views:0, user_id:kanwal.id, created_at:Time.zone.now )
nccu = Pitch.create!( innovation_name:"NCCU Pitch", problem:"Entrepreneurship @ NCCU", pitch:"6C_faIbQHh4", stage:"Concept", needs:"Funding", love:0, feedback_total:0, stars_total:0, stars_avg:0, views:0, user_id:maxime.id, created_at:Time.zone.now )
pitch = Pitch.create!( innovation_name:"Pitch Test", problem:"B Money", pitch:"XYznRbTBuh8", stage:"Idea", needs:"tacos", love:0, feedback_total:0, stars_total:0, stars_avg:0, views:0, user_id:brandon.id, created_at:Time.zone.now )
snackadeez = Pitch.create!( innovation_name:"Snackadeez", problem:"Food options on college campuses", pitch:"c9CFQmP_LDU", stage:"Prototype", needs:"I need help prioritizing next steps", love:0, feedback_total:0, stars_total:0, stars_avg:0, views:0, user_id:ace.id, created_at:Time.zone.now )
pitchlove = Pitch.create!( innovation_name:"PitchLove", problem:"Funding", pitch:"0", stage:"Concept", needs:"Action", love:0, feedback_total:0, stars_total:0, stars_avg:0, views:0, user_id:larry.id, created_at:Time.zone.now )
happy = Pitch.create!( innovation_name:"Happy Hour", problem:"People are not positive enough", pitch:"0", stage:"Concept", needs:"More beta testers", love:0, feedback_total:0, stars_total:0, stars_avg:0, views:0, user_id:maxime.id, created_at:Time.zone.now )
testing1 = Pitch.create!( innovation_name:"Testing1", problem:"Your phone can last a full day charged.", pitch:"PcxCqOK5cuc", stage:"Idea", needs:"Team members to create a prototype", love:99, feedback_total:99, stars_total:381, stars_avg:3.79685, views:203, user_id:maxime.id, created_at:Time.zone.now )
testing2 = Pitch.create!( innovation_name:"Testing2", problem:"I don''t know how to get my app idea started.", pitch:"HEi0U8ZAtNA", stage:"Product/Market Fit", needs:"Larger scale marketing", love:5001, feedback_total:4201, stars_total:3757, stars_avg:2.30037, views:13457, user_id:maxime.id, created_at:Time.zone.now )

Feedback.create!( email:"", frequency:0, feedback:"Watch shows from a laptop hooked up to my TV.", love_flag:-1, stars:3.3, pitch_id:testing3.id, created_at:Time.zone.now )
Feedback.create!( email:"", frequency:0, feedback:"Watch shows from a laptop hooked up to my TV.", love_flag:-1, stars:3.3, pitch_id:testing3.id, created_at:Time.zone.now )
Feedback.create!( email:"", frequency:0, feedback:"Watch shows from a laptop hooked up to my TV.", love_flag:-1, stars:3.3, pitch_id:testing3.id, created_at:Time.zone.now )
Feedback.create!( email:"", frequency:0, feedback:"Watch shows from a laptop hooked up to my TV.", love_flag:-1, stars:3.3, pitch_id:testing3.id, created_at:Time.zone.now )
Feedback.create!( email:"", frequency:0, feedback:"I connect my laptop to my TV and use sites there for feedback.", love_flag:-1, stars:2.8, pitch_id:testing3.id, created_at:Time.zone.now )
Feedback.create!( email:"", frequency:0, feedback:"Have fun and not watch TV.", love_flag:-1, stars:0.8, pitch_id:testing4.id, created_at:Time.zone.now )
Feedback.create!( email:"", frequency:0, feedback:"Have fun and not watch TV.", love_flag:-1, stars:0.8, pitch_id:testing4.id, created_at:Time.zone.now )
Feedback.create!( email:"", frequency:0, feedback:"Testing this out.", love_flag:-1, stars:1.5, pitch_id:wildness.id, created_at:Time.zone.now )
Feedback.create!( email:"sjdhcsdl@none.com", frequency:0, feedback:"Testing this out.", love_flag:-1, stars:1.5, pitch_id:wildness.id, created_at:Time.zone.now )
Feedback.create!( email:"bob@none.com", frequency:0, feedback:"Just having my phone on rest when I''m not using it.", love_flag:-1, stars:2.6, pitch_id:testing1.id, created_at:Time.zone.now )
Feedback.create!( email:"maxime@e2.is", frequency:0, feedback:"A lot of people think they need apps.", love_flag:1, stars:3.7, pitch_id:testing2.id, created_at:Time.zone.now )
Feedback.create!( email:"maxime@e2.is", frequency:0, feedback:"I follow blogs and go to meetups, but there is no one like me there.", love_flag:1, stars:3.7, pitch_id:fndrs.id, created_at:Time.zone.now )
Feedback.create!( email:"", frequency:0, feedback:"This is a test", love_flag:-1, stars:0.5, pitch_id:testing5.id, created_at:Time.zone.now )
Feedback.create!( email:"bob@none.com", frequency:0, feedback:"Nope", love_flag:1, stars:0, pitch_id:fndrs.id, created_at:Time.zone.now )
Feedback.create!( email:"bob@none.com", frequency:0, feedback:"Nope", love_flag:1, stars:0, pitch_id:fndrs.id, created_at:Time.zone.now )
Feedback.create!( email:"Alston.clark17@gmail.com", frequency:0, feedback:"By engaging in meaningful conversation with other minority startup founders", love_flag:1, stars:0, pitch_id:fndrs.id, created_at:Time.zone.now )
Feedback.create!( email:"", frequency:0, feedback:"gygugj", love_flag:-1, stars:3.5, pitch_id:fndrs.id, created_at:Time.zone.now )
Feedback.create!( email:"sjdhcsdl@none.com", frequency:7, feedback:"Magic", love_flag:-1, stars:1.3, pitch_id:testing3.id, created_at:Time.zone.now )
Feedback.create!( email:"sjdhcsdl@none.com", frequency:6, feedback:"Magic", love_flag:-1, stars:1.3, pitch_id:testing3.id, created_at:Time.zone.now )
Feedback.create!( email:"sjdhcsdl@none.com", frequency:6, feedback:"Magic", love_flag:-1, stars:1.3, pitch_id:testing3.id, created_at:Time.zone.now )
