# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150729174528) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "feedbacks", force: :cascade do |t|
    t.string   "email"
    t.integer  "frequency"
    t.text     "feedback"
    t.integer  "love_flag"
    t.integer  "stars"
    t.integer  "pitch_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "feedbacks", ["pitch_id"], name: "index_feedbacks_on_pitch_id", using: :btree

  create_table "pitches", force: :cascade do |t|
    t.string   "innovation_name"
    t.text     "problem"
    t.text     "pitch"
    t.text     "stage"
    t.text     "needs"
    t.integer  "love"
    t.integer  "feedback_total"
    t.integer  "stars_total"
    t.integer  "stars_avg"
    t.integer  "views"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "pitch_id"
  end

  add_index "pitches", ["user_id"], name: "index_pitches_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "new"
    t.integer  "fbid"
    t.string   "funame"
    t.string   "fname"
    t.string   "username"
    t.boolean  "registered"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "feedbacks", "pitches"
  add_foreign_key "pitches", "users"
end
