class AddColumnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :new, :string
    add_column :users, :fbid, :integer
    add_column :users, :funame, :string
    add_column :users, :fname, :string
    add_column :users, :username, :string
  end
end
