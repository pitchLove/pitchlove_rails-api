class CreateInnovations < ActiveRecord::Migration
  def change
    create_table :innovations do |t|
      t.string :innovation_name
      t.text :problem
      t.text :pitch
      t.text :stage
      t.text :needs
      t.integer :love
      t.integer :feedback_total
      t.integer :stars_total
      t.integer :stars_avg
      t.integer :views
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
