class ChangeInnovationIdInFeedbacks < ActiveRecord::Migration
  def change
    rename_column :feedbacks, :innovation_id, :pitch_id
  end
end
