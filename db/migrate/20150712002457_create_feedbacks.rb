class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :email
      t.integer :frequency
      t.text :feedback
      t.integer :love_flag
      t.integer :stars
      t.integer :stars
      t.references :innovation, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
