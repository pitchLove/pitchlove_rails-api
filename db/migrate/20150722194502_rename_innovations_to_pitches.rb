class RenameInnovationsToPitches < ActiveRecord::Migration
  def change
    rename_table :innovations, :pitches
  end
end
