-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 06, 2015 at 05:19 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pitchlove`
--

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `innovationid` int(11) unsigned NOT NULL,
  `email` varchar(50) NOT NULL,
  `frequency` int(11) NOT NULL,
  `feedback` varchar(160) NOT NULL,
  `loveFlag` int(1) NOT NULL,
  `stars` float NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `innovationid` (`innovationid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=131 ;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `innovationid`, `email`, `frequency`, `feedback`, `loveFlag`, `stars`, `timestamp`) VALUES
(102, 2, 'N/A', 0, 'Watch shows from a laptop hooked up to my TV.', -1, 3.3, '2015-04-01 16:14:36'),
(103, 2, 'N/A', 0, 'Watch shows from a laptop hooked up to my TV.', -1, 3.3, '2015-04-01 16:17:53'),
(104, 2, 'N/A', 0, 'Watch shows from a laptop hooked up to my TV.', -1, 3.3, '2015-04-01 16:18:38'),
(105, 2, 'N/A', 0, 'Watch shows from a laptop hooked up to my TV.', -1, 3.3, '2015-04-01 16:19:28'),
(106, 2, 'N/A', 0, 'Watch shows from a laptop hooked up to my TV.', -1, 3.3, '2015-04-01 16:19:46'),
(107, 2, 'N/A', 0, 'Watch shows from a laptop hooked up to my TV.', -1, 3.3, '2015-04-01 16:20:05'),
(108, 2, 'N/A', 0, 'I connect my laptop to my TV and use sites there for feedback.', -1, 2.8, '2015-04-01 20:20:27'),
(109, 5, 'N/A', 0, 'Have fun and not watch TV.', -1, 0.8, '2015-04-01 20:55:28'),
(110, 5, 'N/A', 0, 'Have fun and not watch TV.', -1, 0.8, '2015-04-01 20:56:48'),
(111, 8, 'N/A', 0, 'This is a test', -1, 0.5, '2015-04-01 20:57:04'),
(112, 15, 'N/A', 0, 'Testing this out.', -1, 1.5, '2015-04-01 21:05:51'),
(113, 15, 'sjdhcsdl@none.com', 0, 'Testing this out.', -1, 1.5, '2015-04-01 21:07:26'),
(114, 15, 'sjdhcsdl@none.com', 0, 'Testing this out.', -1, 1.5, '2015-04-01 21:08:13'),
(115, 16, 'bob@none.com', 0, 'Just having my phone on rest when I''m not using it.', -1, 2.6, '2015-04-01 21:15:51'),
(116, 17, 'maxime@e2.is', 0, 'A lot of people think they need apps.', 1, 3.7, '2015-04-01 21:16:26'),
(117, 18, 'maxime@e2.is', 0, 'I follow blogs and go to meetups, but there is no one like me there.', 1, 3.7, '2015-04-01 21:24:54'),
(118, 18, 'maxime@e2.is', 0, 'I follow blogs and go to meetups, but there is no one like me there.', 1, 3.7, '2015-04-01 21:31:28'),
(119, 18, 'maxime@e2.is', 0, 'I follow blogs and go to meetups, but there is no one like me there.', 1, 3.7, '2015-04-01 21:32:45'),
(120, 2, 'sjdhcsdl@none.com', 7, 'Magic', -1, 1.8, '2015-04-10 03:27:19'),
(121, 2, 'sjdhcsdl@none.com', 6, 'Magic', -1, 1.3, '2015-04-10 03:28:35'),
(122, 2, 'sjdhcsdl@none.com', 6, 'Magic', -1, 1.3, '2015-04-10 03:29:08'),
(123, 2, 'sjdhcsdl@none.com', 6, 'Magic', -1, 1.3, '2015-04-10 03:29:51'),
(124, 2, 'sjdhcsdl@none.com', 6, 'Magic', -1, 1.3, '2015-04-10 03:30:15'),
(125, 2, 'sjdhcsdl@none.com', 6, 'Magic', -1, 1.3, '2015-04-10 03:30:31'),
(126, 1, '', 1, 'gygugj', -1, 3.5, '2015-05-18 21:24:12'),
(127, 1, 'Alston.clark17@gmail.com', 0, 'By engaging in meaningful conversation with other minority startup founders ', 1, 0, '2015-05-21 19:01:35'),
(128, 1, 'bob@none.com', 0, 'Nope', 1, 0, '2015-06-17 22:10:20'),
(129, 1, 'bob@none.com', 0, 'Nope', 1, 0, '2015-06-17 22:10:22'),
(130, 1, 'bob@none.com', 0, 'Nope', 1, 0, '2015-06-17 22:10:26');

-- --------------------------------------------------------

--
-- Table structure for table `innovation`
--

CREATE TABLE IF NOT EXISTS `innovation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL,
  `innovation_name` varchar(50) NOT NULL,
  `problem` varchar(140) NOT NULL,
  `pitch` varchar(20) NOT NULL,
  `stage` varchar(50) NOT NULL,
  `needs` varchar(50) NOT NULL,
  `love` int(11) NOT NULL DEFAULT '0',
  `feedbacktotal` int(11) NOT NULL DEFAULT '0',
  `starstotal` int(11) NOT NULL DEFAULT '0',
  `starsavg` float NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `innovation`
--

INSERT INTO `innovation` (`id`, `userid`, `innovation_name`, `problem`, `pitch`, `stage`, `needs`, `love`, `feedbacktotal`, `starstotal`, `starsavg`, `views`, `timestamp`) VALUES
(1, 7, '#000000 fndrs', 'How to connect minority startup founders together for more success.', 'hOV4Gsumb8o', 'Product/Market Fit', 'Funding', 3, 5, 5, 0.700002, 50, '2015-07-06 01:37:01'),
(2, 7, 'Testing3', 'How to find your favorite show when you don''t have cable.', 'HEi0U8ZAtNA', 'Concept', 'Engineers to build prototype.', -2, 27, 64, 4.33125, 34, '2015-06-17 22:10:29'),
(5, 7, 'Testing4', 'How to still support your favorite musicians, but want to know if the songs are good on the CD.', 'dlezSiH_1KY', 'Prototype', 'A strong testing group.', 3, 12, 43, 3.46976, 14, '2015-04-01 20:56:48'),
(8, 7, 'Testing5', 'How to get medicine when you''re sick and can''t get up.', 'djakFYYoOck', 'Idea', 'Need to set up the business', 66, 86, 240, 2.2925, 120, '2015-06-24 17:46:26'),
(15, 7, 'Wildness', 'When you feel bored and need to mix it up.', 'pdFCUx5qXk0', 'Concept', 'More research', -3, 3, 3, 1.5, 5, '2015-06-26 19:36:34'),
(16, 7, 'Testing1', 'Your phone can last a full day charged.', 'PcxCqOK5cuc', 'Idea', 'Team members to create a prototype', 99, 99, 381, 3.79685, 203, '2015-06-26 22:38:24'),
(17, 7, 'Testing2', 'I don''t know how to get my app idea started.', 'HEi0U8ZAtNA', 'Product/Market Fit', 'Larger scale marketing', 5001, 4201, 3757, 2.30037, 13457, '2015-04-01 21:16:26'),
(18, 7, '#000000 fndrs', 'How to connect minority startup founders together for more success.', 'hOV4Gsumb8o', 'Prototype', 'Funding', 3, 3, 3, 3.7, 3, '2015-04-01 21:32:45'),
(20, 7, 'Crunchii - Registration', 'Financial Data prediction', 'https://pitchlove-me', 'Idea', 'Majestic unicorns', 0, 0, 0, 0, 0, '2015-04-06 14:42:09'),
(21, 11, 'tester', 'Water Purification', '0', 'Idea', 'We will purify water using organic material', 0, 0, 0, 0, 0, '2015-04-21 14:38:52'),
(22, 11, 'testertester', 'Water Purification', '0', 'Idea', 'We will purify water using organic material', 0, 0, 0, 0, 0, '2015-04-21 14:44:23'),
(23, 7, 'Testing a pitch for Liz', 'Shows her how the platform works.', 'HvrsTnofKbM', 'Idea', 'More testing.', 0, 0, 0, 0, 0, '2015-04-22 17:01:03'),
(24, 7, 'NCCU Pitch', 'Entrepreneurship @ NCCU', '6C_faIbQHh4', 'Concept', 'Funding', 0, 0, 0, 0, 0, '2015-04-24 16:24:11'),
(25, 14, 'Pitch Test', 'B Money', 'XYznRbTBuh8', 'Idea', 'tacos', 0, 0, 0, 0, 0, '2015-05-18 21:23:01'),
(26, 15, 'Snackadeez', 'Food options on college campuses ', 'c9CFQmP_LDU', 'Prototype', 'I need help prioritizing next steps ', 0, 0, 0, 0, 0, '2015-05-21 18:59:35'),
(27, 7, 'Happy Hour', 'People are not positive enough', '0', 'Concept', 'More beta testers', 0, 0, 0, 0, 0, '2015-05-22 05:34:54'),
(28, 7, 'sajdfhdalfk', 'dflksdhflkvj', '0', 'Prototype', 'sdlkcjsdk;cdjs;', 0, 0, 0, 0, 0, '2015-05-22 05:47:16'),
(29, 17, 'PitchLove', 'Funding', '0', 'Concept', 'Action', 0, 0, 0, 0, 0, '2015-05-22 15:27:33'),
(30, 7, 'I''m just testing', 'Does this test actually work?', 'aTN6rUKPKew', 'Scaling', 'Trying to make everyone use this.', 0, 0, 0, 0, 0, '2015-05-22 15:37:22'),
(31, 7, 'sdfl;sdfj', 'sldkvjdsfklj', '0', 'Prototype', 'sdlkfjdsf;ljsd;lvs', 0, 0, 0, 0, 0, '2015-05-28 17:00:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `new` varchar(1) NOT NULL,
  `fbid` int(11) NOT NULL,
  `funame` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `femail` varchar(255) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `new`, `fbid`, `funame`, `fname`, `femail`, `username`, `email`, `timestamp`) VALUES
(7, '', 2147483647, 'Maxime', 'Maxime Paul', 'mightyricemax21@gmail.com', 'maxap', 'maxime@e2.is', '2015-06-29 23:48:41'),
(9, '', 2147483647, 'Maxime', 'Maxime Paul', 'mightyricemax21@gmail.com', NULL, '', '2015-06-29 23:48:41'),
(10, '', 2147483647, 'Stephanie', 'Stephanie L. Ghoston', 'slghosto@gmail.com', 'slghosto', 'slghosto@gmail.com', '2015-04-15 14:45:39'),
(11, '', 2147483647, 'Kanwal', 'Kanwal Fatima', 'shumailb9@gmail.com', 'tester', 'shumailmb@yahoo.com', '2015-04-21 20:04:04'),
(12, '', 2147483647, 'Julius', 'Julius Rainey', 'juliusrainey@gmail.com', 'one9ooh6', 'juliusrainey@gmail.com', '2015-04-26 23:15:11'),
(13, '', 2147483647, 'Tobias', 'Tobias Rose', 'tobias@komplekscreative.com', NULL, '', '0000-00-00 00:00:00'),
(14, '', 2147483647, 'Brandon', 'Brandon T. Luong', 'brandontluong@gmail.com', 'assmaster', 'brandontluong@gmail.com', '2015-05-18 21:21:31'),
(15, '', 2147483647, 'Ace', 'Ace Clark', 'arcticnanuk@gmail.com', 'Alston', 'alston.clark17@gmail.com', '2015-05-21 18:20:41'),
(16, '', 2147483647, 'Liz', 'Liz Akins', 'elizabeth.akinsanmi@howard.edu', 'dark_blaze', 'Lizakins12@gmail.com', '2015-05-22 06:50:51'),
(17, '', 2147483647, 'Larry', 'Larry Jr.', 'larrywilliamsjr87@gmail.com', 'poweruser', 'larrywilliamsjr87@gmail.com', '2015-05-22 15:24:37'),
(18, '', 2147483647, 'Brian', 'Brian Keith Lee', 'bkl82@me.com', 'briguy82', 'bkl82@me.com', '2015-06-17 23:11:46'),
(19, '', 2147483647, 'Nic', 'Nic Small', 'nas231@nyu.edu', 'Nic', 'nic@smallnic.com', '2015-07-04 20:06:09');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`innovationid`) REFERENCES `innovation` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `innovation`
--
ALTER TABLE `innovation`
  ADD CONSTRAINT `innovation_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
