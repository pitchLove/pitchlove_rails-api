Rails.application.routes.draw do
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  root 'welcome#index'

  get 'create', to: 'pitches#new', as: 'create'

  # resources :pitches
  resources :users, only: [:index, :update]
  resources :feedbacks, only: [:index]

  # Creating a RESTful API with versioning
  # http://railscasts.com/episodes/350-rest-api-versioning?view=asciicast
  # http://stackoverflow.com/questions/13750188/rails-namespace-routes-and-custom-action-on-resource

  namespace :api do
    namespace :pitchlove do

      resources :pitches do
        collection do
          get "all"
          post "create"
          post "data"
          # match 'api/pitchlove/pitches/all', :to => 'pitches#all', :via => "get"
          # match 'api/pitchlove/pitches/:id', :to => 'pitches#show', :via => "get"
          # match 'api/pitchlove/pitches/new', :to => 'pitches#new', :via => "get"
          # match 'api/pitchlove/pitches/create', :to => 'pitches#create', :via => "post"

          # match ':id', :to => 'pitches#show', :via => "get"
          # match 'new', :to => 'pitches#new', :via => "get"
          # match 'pitches', :to => 'pitches#create', :via => "post"
        end
      end

      resources :users do
        collection do
          get "all"
          post "signin"
        end
      end

      resources :feedbacks do
        collection do
          get "all"
        end
      end
    end
  end

  # match 'api/pitchlove/pitches/all', :to => 'pitches#all'
  # match 'api/pitchlove/pitches/:id', :to => 'pitches#show'
  # match 'api/pitchlove/pitches/new', :to => 'pitches#new'
  # match 'api/pitchlove/pitches/create', :to => 'pitches#create', :via => "post"


  # resources :pitches do
  #   get 'api/pitchlove/pitches/all' => "pitches#all"
  # end

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end



end
