
$(document).ready(function(){
  console.log("jQuery is running")

  /* Signup */

  $(".signup,.login-links-r a").click(function () {
    $(".panel-pop").animate({"top":"-100%"},10).hide();
    $("#signup").show().animate({"top":"50%"},500);
    $("body").prepend("<div class='wrap-pop'></div>");
    wrap_pop();
    return false;
  });

  /* Signin */

  $(".signin,.login-links-r a").click(function () {
    $(".panel-pop").animate({"top":"-100%"},10).hide();
    $("#signin").show().animate({"top":"50%"},500);
    $("body").prepend("<div class='wrap-pop'></div>");
    wrap_pop();
    return false;
  });

  /* Panel pop */

	$(".panel-pop").each(function () {
		var panel_pop = $(this);
		var panel_height = panel_pop.height();
		panel_pop.css("margin-top","-"+panel_height/2+"px");
	});

	$(".panel-pop h2 i").click(function () {
		$(this).parent().parent().animate({"top":"-100%"},500);
		$(".wrap-pop").remove();
	});

	function wrap_pop() {
		$(".wrap-pop").click(function () {
			$(".panel-pop").animate({"top":"-100%"},500).hide(function () {
				$(this).animate({"top":"-100%"},500);
			});
			$(this).remove();
		});
	}

})
