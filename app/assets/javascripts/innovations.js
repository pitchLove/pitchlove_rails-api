$("#not").click(function(){
    $("#love").hide();
  })

function reqListener () {
      console.log(this.responseText);
    }

var oReq = new XMLHttpRequest(); //New request object
oReq.onload = function() {
    //This is where you handle what to do with the response.
    //The actual data is found on this.responseText

    $("#innovation").change(function() {
        if(this.checked){
            $("div.company").find("input").prop('required',true);
            $("div#explain").hide()
            $( "#fillin").html("Fill in the information about your innovation below and save your Profile.");
            $('#innovation').value = true;
        } else {
           $('#innovation').value = false;
        }
    });
    alert(this.responseText); //Will alert: 42
};
oReq.open("GET", "inc/innovations.php?t=" + Math.random(), true);
//                               ^ Don't block the rest of the execution.
//                                 Don't wait until the request finishes to
//                                 continue.
oReq.send();
