class FeedbacksController < ApplicationController

  def index
    @feedbacks = Feedback.all
    render status: 200, json: @feedbacks.to_json
  end


end
