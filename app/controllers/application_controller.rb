class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  helper_method :pitch_id
  helper_method :getNextPitch
  helper_method :checkRegistration


  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :new
    devise_parameter_sanitizer.for(:sign_up) << :fbid
    devise_parameter_sanitizer.for(:sign_up) << :funame
    devise_parameter_sanitizer.for(:sign_up) << :fname
    devise_parameter_sanitizer.for(:sign_up) << :username
    devise_parameter_sanitizer.for(:sign_up) << :registered

  end

  def getHashId(pitch_id)
    hashids = Hashids.new("pitchlove") #this unique salt value makes it so our hashes differ from everyone else's
    return hashids.encode(pitch_id)
  end

  def getNextPitch()
    return Pitch.all.sample.id
  end

  def checkRegistration(email, username, password)
    user = User.find_by(email: email) || User.find_by(username: username)

    if (user.valid_password?(password))
      return {username:user.username, registered:user.registered}
    else
      return {username: username, registered: false}
    end
  end

end
