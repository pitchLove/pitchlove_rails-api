module Api
  module Pitchlove
    class FeedbacksController < ApplicationController

      def all
        @feedbacks = Feedback.all
        render status: 200, json: @feedbacks.to_json
      end

      def new
        @feedback = Feedback.new()
        render 'new'
      end

      def create
        binding.pry
        @feedback = Feedback.new(feedbacks_params)
        @feedback.save
        render status: 200, json: @feedback.to_json(:only => [:email, :feedback, :stars, :love_flag],
                                                     :include => {:pitches => {
                                                                  :only => [:stars_avg, :love]}},
                                                     :method => :getNextPitch
                                                    )
      end

      # Method that gets feedback data and saves in DB then send new pitch when successful - POST /api/pitchlove/feedback/:id: SEND pitch id, feedback info: RETURN successfully added feedback
      # Email <Feedback table>
      # Feedback message <Feedback table>
      # Avg Rating <Pitch table>
      # Rating <Feedback table>
      # Love <Pitch table>
      # Vote <Feedback table>
      # Randomly pick next pitch and send new pitch back
      # Get new id and call method #3

      # t.string   "email"
      # t.integer  "frequency"
      # t.text     "feedback"
      # t.integer  "love_flag"
      # t.integer  "stars"
      # t.integer  "pitch_id"

      private
      def feedbacks_params
        params.require(:feedback).permit(:email, :feedback, :stars, :love_flag, :pitch_id)
      end

    end
  end
end
