 module Api
   module Pitchlove
      class PitchesController < ApplicationController

        skip_before_filter :verify_authenticity_token


        def all
          @pitches = Pitch.all
          render status: 200, json: @pitches.to_json(:only => [:innovation_name, :problem, :pitch, :id ])
        end

        def show
          @pitch = Pitch.find(params[:id])
          # @object.to_json(:except => ['created_at', 'updated_at'], :methods => ['method1', 'method2'])
          # binding.pry
          # hash = hashids.encode(12345)
          # @pitch.id = hashids.encode(@pitch.id)
          @pitch.pitch_id = getHashId(@pitch.id)
          render status: 200, json: @pitch.to_json(:only => [ :innovation_name, :problem, :pitch, :pitch_id ])
          # (:methods => :permalink)
        end

        def new
          @pitch = Pitch.new()
          render '/pitches/new'
        end

        def create
          # binding.pry
          @user = User.find_by(username:params[:username])
          @pitch = Pitch.new(innovation_name:params[:name], problem:params[:problem], stage:params[:stage], needs:params[:needs], pitch:params[:vid_id])
          @pitch.user_id = @user.id
          @pitch.save
          # render status: 200, json: Pitch.all.to_json(:only => [:innovation_name, :problem, :pitch, :id ])
          render status: 200, json: @pitch.to_json(:only => [ :innovation_name, :problem, :pitch, :pitch_id ])

        end

        def data
          binding.pry
          username = params[:username]
          registered = params[:registration_flag]
          @pitches = []
          if registered
            user = User.find_by(username:params[:username])
            Pitch.all.each do |pitch|
              if pitch.user_id == user.id
                @pitches.push(pitch)
              end
            end
          end

          render status: 200, json: @pitches.to_json(:only => [:innovation_name, :stage, :id, :views, :love, :stars_avg ])

        end


        # Method that saves inputs for pitch in DB and send back successful creation flag - POST /api/pitchlove/pitches/create: send username, registration flag, and new pitch info: return success creation
        # Username
        # Name
        # Problem
        # Stage
        # Needs
        # Logo URL
        # Vid id

        # create_table "pitches", force: :cascade do |t|
        #   t.string   "innovation_name"
        #   t.text     "problem"
        #   t.text     "pitch"
        #   t.text     "stage"
        #   t.text     "needs"
        #   t.integer  "love"
        #   t.integer  "feedback_total"
        #   t.integer  "stars_total"
        #   t.integer  "stars_avg"
        #   t.integer  "views"
        #   t.integer  "user_id"
        #   t.string   "pitch_id"
        # end

        private
        def pitches_params
          params.require(:pitch).permit(:innovation_name, :problem, :pitch, :stage, :needs, :love, :user_id)
        end

      end
   end
end
