module Api
  module Pitchlove
    class UsersController < ApplicationController


      skip_before_filter :verify_authenticity_token

      # def all
      #   @users = User.all
      #   render status: 200, json: @users.to_json
      # end

      def signin
        # binding.pry
        registrationStatus = checkRegistration(params[:email], params[:username], params[:password])
        render status:200, json: registrationStatus.to_json
        # redirect_to '/create'
      end

      def edit
        @user = current_user
        render 'users/edit'
      end

      def update
        @user = User.find(current_user.id)
        @user.update!(user_params)
        @user.save
        redirect_to('/create')
      end

      private
      def user_params
        return params[:user].permit(:username, :email, :user_id, :password)
      end

    end
  end
end
