class UsersController < ApplicationController

  def index
    @users = User.all
    render status: 200, json: @users.to_json
  end

  def update
    @user = User.find(current_user.id)
    @user.update!(user_params)
    @user.save
    redirect_to('/create')
  end

  private
  def user_params
    return params[:user].permit(:username, :email, :user_id)
  end

end
